# Specs #
Processor: aarch64
Linux: Manjaro (Arch-like)

sudo pacman -S libx11 libxext libxtst golang-github-golang-freetype mesa-utils mesa glu pulseaudio alsa-lib pulseaudio-alsa bzip2 lbzip2 libogg ffmpeg live-media sdl2_ttf lame libtheora libvorbis libvpx opus sdl2 sdl2_ttf x264 x265 xvidcore yasm gcc make openssl live-media pkgconf

halted: pulseaudio
	gcc-c++


# changes made to compile in aarch (manjaro)
`File`: Makefile.def (base/ga/Makefile.def)
`change`: added '-fpermissive' in CXXFLAGS and CFLAGS

`File`: Makefile (base/ga/client/Makefile)
`change`: added '-lss -lcrypto' in LDFLAGS
